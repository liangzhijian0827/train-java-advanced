package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class StudentDaoDatabase implements StudentDao {

	@Override
	public boolean save(Student s) throws ClassNotFoundException, SQLException   {
		Integer id=s.getId();
		String name=s.getName();
		Character gender=s.getGender();
		LocalDate birthdate=s.getBirthdate();
		java.sql.Date date = java.sql.Date.valueOf(birthdate);
		
		String driverClassName = "com.mysql.cj.jdbc.Driver";
		String params = "?serverTimezone=Asia/Shanghai&useSSL=false&useServerPrepStmts=true&cachePrepStmts=true";
		String url = "jdbc:mysql://localhost:3306/ecut" + params;
		String user = "ecuter";
		String password = "ecuter";
		
		Class.forName(driverClassName);
		Connection connection = DriverManager.getConnection(url, user, password);
		String SQL = "INSERT INTO t_students ( id , name , gender , birthdate ) VALUES ( ? , ? , ? , ? )";
		PreparedStatement ps=null;
		try {
			ps = connection.prepareStatement(SQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ps.setInt(1,id);
		ps.setString(2, name);
		ps.setString(3, gender.toString());
		ps.setDate(4, date);
		
		int count = ps.executeUpdate();
		System.out.println(count);
		connection.close();
		
		return count==1;
	}

	@Override
	public Student find(Integer id) throws ClassNotFoundException, SQLException {
		String driverClassName = "com.mysql.cj.jdbc.Driver";
		String params = "?serverTimezone=Asia/Shanghai&useSSL=false&useServerPrepStmts=true&cachePrepStmts=true";
		String url = "jdbc:mysql://localhost:3306/ecut" + params;
		String user = "ecuter";
		String password = "ecuter";
		
		Class.forName(driverClassName);
		Connection connection = DriverManager.getConnection(url, user, password);
		String select = "SELECT id , name , gender , birthdate FROM t_students WHERE id = ? " ;
		PreparedStatement ps=null;
		try {
			ps = connection.prepareStatement(select);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ps.setInt(1,id);
		ResultSet rs = ps.executeQuery();
		while( rs.next() ) { // if( rs.next() ) {
			System.out.print( rs.getInt( "id" ) + "\t" );
			System.out.print( rs.getString( "name" ) + "\t" );
			System.out.print( rs.getString( "gender" ) + "\t" );
			System.out.println( rs.getDate( "birthdate" ) );
		}
		rs.close(  );
		ps.close();
		connection.close();
		
		return null;
	}
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		StudentDaoDatabase sd=new StudentDaoDatabase();
		
		LocalDate date=LocalDate.of(2000, 10, 30);
		Student s=new Student(1,"张三丰",'男',date);
		
		LocalDate date2=LocalDate.of(1999, 8, 2);
		Student s2=new Student(2,"Saltroy",'女',date2);
		
//		sd.save(s2);
		
//		sd.find(2);
		
	}

}
