package files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Comparator;

public class FileCombiner {
	  
	  private Comparator<File> comparator ;
	  
	  private FileCombiner(){
	    super();
	  }
	  
	  /**
	   * 用来获取 文件合并器 的 静态工厂方法
	   * @param comparator 表示用来对列表中的文件进行排序的比较器
	   */
	  public static FileCombiner getInstance( Comparator<File> comparator ){
	    FileCombiner fcb=new FileCombiner();
	    fcb.comparator=comparator;
	    return fcb;
	  }
	  
	  public static FileCombiner getInstance(){
	    Comparator<File> comparator = new Comparator<>(){
	      // 实现 compare 方法并根据 File 名称排序
	    	@Override
	        public int compare(File o1, File o2) {
	    		String name1=o1.getName();
	           int index1=name1.indexOf(".");
	           String o1name=name1.substring(index1-2, index1);
	           
	           String name2=o2.getName();
	           int index2=name1.indexOf(".");
	           String o2name=name2.substring(index2-2, index2);
	           
	           int x=0;
	           if(o1name.compareTo(o2name)>0) {
	        	   x=1;
	           }
	           if(o1name.compareTo(o2name)<0) {
	        	   x= -1;
	           }
	           if(o1name.compareTo(o2name)==0) {
	        	   x= 0;
	           }
			return x;
	        }
	    };
	    
	    return getInstance( comparator ); // 调用另外一个 getInstance 方法
	  }
	  
	  /**
	   * 将第一个参数对应的目录下的文件合并后，保存到第二个参数对应的位置
	   * @param path 需要被合并的众多小文件的存放目录
	   * @param dest 合并后的大文件的存放位置
	 * @throws IOException 
	   */
	  public void combine( File path , File dest ) throws IOException{
	    
	    // 获取到文件列表后，要对文件列表进行排序 ( 使用 comparator 完成排序 )
		  if( !path.exists() || !path.isDirectory() ) {
				throw new IllegalArgumentException( "等待合并的文件路径不正确" );
			}
			
			if( !dest.exists() || !dest.isDirectory() ) {
				throw new IllegalArgumentException( "合并后的路径不正确" );
			}
			
			FilenameFilter fileNameFilter = new FilenameFilter() {
				@Override
				public boolean accept( File dir, String name ) {
					
					String prefix = "" ;
					String suffix = "" ;
					
					int index = name.indexOf( "." );
					if( index > 0 ) {
						prefix = name.substring( 0 , index );
						suffix = name.substring( index );
					} else {
						prefix = name ;
					}
					
					if( name.charAt(name.length()-suffix.length()-1)>='0'&& name.charAt(name.length()-suffix.length()-1)<='9' ) {
						return true ;
					}
					return false;
				}
			};
			
			File[] files=path.listFiles(fileNameFilter);
			for(int i=0;i<files.length-1;i++) {
				for(int j=i+1;j<files.length;j++) {
					if(comparator.compare(files[i], files[j])==1) {
						File temp=files[j];
						files[i]=files[j];
						files[j]=temp;
					}
				}
			}
			String onename=files[0].getName();
			
			String prefix = "" ;
			String suffix = "" ;
			
			int index = onename.indexOf( "." );
			if( index > 0 ) {
				prefix = onename.substring( 0 , index -2);
				suffix = onename.substring( index );
			} else {
				prefix = onename ;
			}
			
			String newname = prefix+suffix;
			for(int i = 0;i<files.length;i++) {
				InputStream in = new FileInputStream( path+"/"+files[i].getName() ) ;
				final byte[] buffer = new byte[1024];			
							
				int p ;	
				while( ( p = in.read( buffer ) ) != -1 ) { 
					File nf = new File("NEW"+newname);
					OutputStream out = new FileOutputStream( dest+"/"+nf , true );
					out.write(buffer);
					out.close();
				}
				in.close();
			}
			
	  }
	  
	  public static void main( String[] args ) throws IOException {
	    
	    File path = new File("E:/test/" ) ;
	    File dest = new File( "E:/test/" ) ;
	    
	    FileCombiner fc = FileCombiner.getInstance(); // 默认根据文件名称对文件列表排序
	    
	    fc.combine( path , dest );
	    
	  }

	}
