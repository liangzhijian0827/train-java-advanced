package files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileSplitter {
	  
	  private int size ;
	  
	  private FileSplitter(){
	    super();
	  }
	  
	  /**
	   * 用来获取 文件切割器 的 静态工厂方法
	   * @param size 表示被拆分后的单文件最大体积
	   */
	  public static FileSplitter getInstance( int size ){
		  FileSplitter fsp=new FileSplitter();
		  fsp.size=size;
	    return fsp;
	  }
	  
	  /**
	   * 用来获取 文件切割器 的 静态工厂方法
	   * @param origin 表示需要被拆分的文件
	   * @param destination 表示拆分后的每份文件的存放目录
	   * @return 返回拆分后的文件数目
	 * @throws IOException 
	   */
	  public int split( File origin , File destination ) throws IOException {
	    // 在这里完成文件拆分操作
		  if( !origin.exists() || !origin.isFile() ) {
				throw new IllegalArgumentException( "被拆分的文件不存在" );
			}
			
			if( !destination.exists() || !destination.isDirectory() ) {
				throw new IllegalArgumentException( "目标位置不存在或不有效目录" );
			}
			final long length=origin.length();
			final String name=origin.getName();
			
			String prefix = "" ;
			String suffix = "" ;
			
			int index = name.indexOf( "." );
			if( index > 0 ) {
				prefix = name.substring( 0 , index );
				suffix = name.substring( index );
			} else {
				prefix = name ;
			}
			
			long n = length / size;
			if( length % size != 0 ) {
				n++ ; 
			}
			
			InputStream in=new FileInputStream(origin);
			
			final byte[] buffer=new byte[this.size];
			
			for( int i = 1 ; i <= n ; i++ ) {
				String filename = prefix + "-" + ( i < 10 ? ( "0" + i ) : i ) + suffix ;
				
				int x = in.read( buffer ); 
				
				File dest = new File( destination , filename );
				OutputStream out = new FileOutputStream( dest );
				out.write( buffer ,  0 , x ); 
				out.close();
			}
			in.close();
			return (int)n;
	  }

	  public static void main( String[] args ) throws IOException {
	    
	    File origin = new File( "E:/test/chuanqi.mp3" ) ;
	    File dest = new File( "E:/test/" ) ;
	    
	    int size = 1 << 20 ;
	    
	    FileSplitter fs = FileSplitter.getInstance( size );
	    
	    int n = fs.split( origin , dest );
	    System.out.println( "拆分后的文件数目为: " + n );
	  }

	}