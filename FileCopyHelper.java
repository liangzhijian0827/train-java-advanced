package files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;



public class FileCopyHelper {
	
	/**
	 * 使用 传统IO 实现文件复制 
	 * @param src 被复制文件
	 * @param dst 复制后的文件
	 */
	public static void copy( File src , File dst ) throws IOException {
		final long begin = System.nanoTime(); // 返回毫微秒 (不是从历元开始计时)
		if( !src.exists() || !src.isFile() ) {
			throw new IllegalArgumentException( "被复制的文件不存在" );
		}
		// 借助于 FileInputStream 和 FileOutputStream 实现文件复制
		InputStream in=new FileInputStream(src);
		Reader reader=null;
		reader=new InputStreamReader(in);
		int c;
		while((c=reader.read())!=-1) {
			OutputStream out=new FileOutputStream(dst,true);
			Writer w=null;
			w=new OutputStreamWriter(out);
			w.write(c);
			w.close();
			out.close();
		}
		reader.close();
		in.close();
		
		final long end = System.nanoTime(); 
		System.out.println( "复制文件用时: " + ( end - begin ) + "ns." );
	}
	
	/**
	 * 使用 Channel 和 Buffer 完成文件复制
	 * @param src 被复制的文件路径
	 * @param dst 复制后的文件存放路径
	 * @throws IOException 
	 */
	public static void copy( Path src , Path dst ) throws IOException {
		final long begin = System.nanoTime(); // 返回毫微秒 (不是从历元开始计时)
	
		// 借助于 FileChannel 和 ByteBuffer 实现文件复制
		FileChannel channel=FileChannel.open(src);
		FileChannel channel2=FileChannel.open(dst,StandardOpenOption.WRITE , StandardOpenOption.CREATE);
		final ByteBuffer buffer=ByteBuffer.allocate(100);
		while((channel.read(buffer))!=-1) {
			buffer.flip();
			channel2.write(buffer);
			buffer.clear();
		}
		channel.close();
		channel2.close();
		
		final long end = System.nanoTime(); 
		System.out.println( "复制文件用时: " + ( end - begin ) + "ns." );
	}

}
