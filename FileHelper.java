package files;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class FileHelper {
	
	/**
	 * 将第一个参数传入的文件拆分后依次序存储到第二个参数对应的目录下
	 * @param file 被拆分的源文件
	 * @param path 拆分后的问价存储路径
	 * @param size 表示每个小文件的最大体积
	 * @throws IOException 
	 */
	public static void split( File file , File path , int size ) throws IOException {
		
		if( !file.exists() || !file.isFile() ) {
			throw new IllegalArgumentException( "被拆分的文件不存在" );
		}
		
		if( !path.exists() || !path.isDirectory() ) {
			throw new IllegalArgumentException( "目标位置不存在或不有效目录" );
		}
		
		InputStream in=new FileInputStream(file);
		
		String name=file.getName();
		final byte[] buffer=new byte[size];
		int count=1;
		while(true) {
			int n=in.read(buffer);
			
			if(n==-1) {
				break;
			}
			String s=new String(buffer,0,n);
			byte[] bytes=s.getBytes();
			System.out.println(bytes);
			File dest=new File(path+"/"+name+count);
			OutputStream out=new FileOutputStream(dest);
			out.write(bytes);
			count++;
			out.close();
			
 		}
		in.close();
		
		// 从 file 所表示的文件中读取数据，每次读取 50MB ，写出到一个新文件中
		// 假设大文件名称为 阿凡达.mp4 ( 1576MB ) ，则拆分后的文件为 "阿凡达01" 、 "阿凡达02" ...
		
	}
	
	/**
	 * 将第一个参数传入的目录中有序的文件合并到一个大文件( 第二个参数表示合并后的文件的存储位置 )
	 * @param path 依次序存放了一批等待合并的小文件的目录
	 * @param dest 合并之后的大文件的存储路径
	 * @throws IOException 
	 */
	public static void join( File path , File dest ) throws IOException {
		if( !path.exists() || !path.isDirectory() ) {
			throw new IllegalArgumentException( "等待合并的文件路径不正确" );
		}
		
		if( !dest.exists() || !dest.isDirectory() ) {
			throw new IllegalArgumentException( "合并后的路径不正确" );
		}
		
		FilenameFilter fileNameFilter = new FilenameFilter() {
			@Override
			public boolean accept( File dir, String name ) {
				if( name.charAt(name.length()-1)>='0'&& name.charAt(name.length()-1)<='9' ) {
					return true ;
				}
				return false;
			}
		};
		String[] itemNames = path.list(fileNameFilter);
		String newname = itemNames[0].substring(0,itemNames[0].length()-1);
		for(int i = 0;i<itemNames.length;i++) {
			InputStream in = new FileInputStream( path+"/"+itemNames[i] ) ;
			final byte[] buffer = new byte[1024];			
						
			int p ;	
			while( ( p = in.read( buffer ) ) != -1 ) { 
				File nf = new File("NEW"+newname);
				OutputStream out = new FileOutputStream( path+"/"+nf , true );
				out.write(buffer);
				out.close();
			}
			in.close();
		}
	}

}
